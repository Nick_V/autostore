<?php
namespace app\models;
use yii\base\Model;

class CategoryForm extends Model
{
    public $cat_name;
    public $parent_id;
    
    public function rules() {
        return [
            [['cat_name', 'parent_id'], 'required'],
            ['cat_name', 'string', 'max' => 255 ],
        ];
    }
    
}


