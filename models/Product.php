<?php
namespace app\models;
use yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

class Product extends ActiveRecord {
    
    public static function tableName()
    {
        return 'product';        
    }
    
    public static function SearchAll()
    {
        return new ActiveDataProvider([
            'query' => Product::find(),
            'pagination' => ['pageSize' => 10,],
            ]);
        
    }
    
    public static function Search($product)
    {
        return new ActiveDataProvider([
            'query' => Product::find()->where(['brand' => $product]),
            'pagination' => ['pageSize' => 10,],
            ]);
    }
  
}
