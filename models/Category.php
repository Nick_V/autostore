<?php
namespace app\models;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;

class Category extends ActiveRecord
{
    public static function tableName()
    {
        return 'category';        
    }
 
    public static function SortArr($nodes)
    {
        $sortarr = array();
        if(!$nodes)
        {
            echo 'not nodes';
        }
        else if(count($nodes) > 0)
        {
            foreach ($nodes as $category)
            {
                if(empty($sortarr[$category['parent_id']]))
                {
                    $sortarr[$category['parent_id']] = array();
                }
                $sortarr[$category['parent_id']][] = $category;
            }
            return $sortarr;
        }
    }
    
    public static function ShowTree($arr, $parent_id = 0)
        {
            if(empty($arr[$parent_id]))
            {
                return;
            }
                        
            echo '<ul>';
            
            for($i = 0; $i < count($arr[$parent_id]); $i++)
            {
                echo '<li>';
                echo Html::a($arr[$parent_id][$i]['cat_name'],['admin/getproducts', 'product' => $arr[$parent_id][$i]['cat_name']]);
                echo '</li>';
                Category::ShowTree($arr, $arr[$parent_id][$i]['id']);
            }
            echo'</ul>';
        }
}

 
