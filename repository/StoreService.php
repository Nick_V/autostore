<?php
namespace app\repository;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Product;
use app\models\Category;
use yii\db\Exception;
use SplFileInfo;

require 'IStoreService.php';

class StoreService implements IStoreService
{
    public function UploadFile() {
        if(Yii::$app->request->post())
        {
            $file = $_FILES['myfile']['name'];
            $upload_dir = '../uploads/';
            $upload_file = $upload_dir.basename($file);
            
            if(move_uploaded_file($_FILES['myfile']['tmp_name'], $upload_file))
            {
                return "<br>".'File '.$file.' upload successful';
            } 
            else 
            {
                return "<br>".'Error upload file!';
            }
        }
    }
    
    public function GetFiles() 
    {
        $listfiles = array();
        $upload_dir = '../uploads/';
        $od = opendir($upload_dir);
        while($filename = readdir($od))
        {
            if(!is_dir($filename))
            {
                $listfiles[] = $filename;
            }
        }
        return $listfiles;
    }
    
    public function Initialise($data, $arr, $brand)
    {
        if(count($arr) == 0)
        {
            StoreService::AddProduct($data);
            $arr = StoreService::GetAllProductsByBrand($brand);
            return $arr;
        }
        return $arr;
    }
    
    public function Update($file)
    {
        $filepath = '../uploads/'.$file;
        $info = new SplFileInfo($file);
        $filetype = $info->getExtension(); 
        
        if($filetype == 'csv')
        {
            $arrCsv = array();
            $brand = basename($file, ".csv");
            $arrCsv = StoreService::GetAllProductsByBrand($brand);
            $handle = fopen($filepath, "r");
           
            while(($data = fgetcsv($handle, 1000, ",")) !== false)
            {
                $arrCsv = StoreService::Initialise($data, $arrCsv, $brand);
                for($i = 0; $i < count($arrCsv); $i++)
                {
                    if($arrCsv[$i]['name'] == $data[2] && $arrCsv[$i]['price_prod'] != $data[0])
                    {
                        $product = Product::find()->where(['name' => $data[2]])->one();
                        $product->price_prod = $data[0];
                        $product->save();
                    }
                    else if(StoreService::IsProductExist($data[2], $arrCsv) == false)
                    {
                        StoreService::AddProduct($data);
                        $arrCsv = StoreService::GetAllProductsByBrand($brand);
                    }
                }
            }
            fclose($handle);
        }
        else if($filetype == 'xlsx')
        {
            $brand = basename($file, ".xlsx");
            $arrExcel = StoreService::GetAllProductsByBrand($brand);
            $dataExcel = array();
            try
            {   $inputFileType = \PHPExcel_IOFactory::identify($filepath);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objExcel = $objReader->load($filepath);
            }
            catch(Exception $e)
            {
                die("error");
            }
            $dataExcel = $objExcel->getActiveSheet()->toArray();
            
            foreach ($dataExcel as $data_coll)
            {
                $newproduct = [ 0 => $data_coll[2], 1 => $data_coll[0], 2 => $data_coll[1]];
                $arrExcel = StoreService::Initialise($newproduct, $arrExcel, $brand);
                for($i = 0; $i < count($arrExcel); $i++)
                {
                    if($arrExcel[$i]['name'] == $data_coll[1] && $arrExcel[$i]['price_prod'] != $data_coll[2])
                    {
                        $product = Product::find()->where(['name' => $data_coll[1]])->one();
                        $product->price_prod = $data_coll[2];
                        $product->save();
                    }
                    else if(StoreService::IsProductExist($data_coll[1], $arrExcel) == false)
                    {
                        StoreService::AddProduct($newproduct);
                        $arrExcel = StoreService::GetAllProductsByBrand($brand);
                    }
                }
            }  
        }
    }
    public function UpdateAll() 
    {
        $files = StoreService::GetFiles();
        foreach($files as $file)
        {
            StoreService::Update($file);
        }
        echo 'update all';
    }


    public function GetCategoryId($brand) {
        return Category::find()->where(['cat_name' => $brand])->one()->id;
    }
    
    public function GetAllCategories()
    {
        $categories = Category::find()->asArray()->all();
        $sortarr = Category::SortArr($categories);
        return Category::ShowTree($sortarr);
    }
    
    public function ArrCategories()
    {
       $categories = Category::find()->asArray()->all();
       $arr = array();
        
        foreach ($categories as $cat)
        {
            $arr[$cat['id']] = $cat['cat_name'];
        }
        return $arr;
    }
    
    public function AddProduct($data) {
        $product = new Product();
        $product->price_prod = $data[0];
        $product->brand = $data[1];
        $product->name = $data[2];
        $product->dateofmanuf = date('Y-m-d H:i:s');
        $product->category_id = StoreService::GetCategoryId($data[1]);
        $product->save();
    }
    
    public function AddCategory($data) {
        $category = new Category();
        $category->cat_name = $data[1];
        $category->parent_id = $data[2];
        $category->save();
    }
   
    public function GetAllProductsByBrand($brand) {
        return Product::find()->asArray()->where(['brand' => $brand])->all();
    }
    
    public function IsProductExist($name, $arr) {
        $arrNames = array();
        for($i = 0; $i < count($arr); $i++)
        {
            $arrNames[] = $arr[$i]['name'];
        }
        return in_array($name, $arrNames);
    }
    
}
