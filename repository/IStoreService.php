<?php
namespace app\repository;

interface IStoreService 
{
    public function UploadFile();
    public function GetFiles();
    public function AddProduct($data);
    public function AddCategory($data);
    public function ArrCategories();
    public function Update($file);
    public function GetCategoryId($brand);
    public function IsProductExist($name, $arr);
    public function Initialise($data, $arr, $brand);
    public function UpdateAll();
    public function GetAllCategories();
    public function GetAllProductsByBrand($brand);
}
