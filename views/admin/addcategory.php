<?php
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use app\models\Product;
use app\models\Category;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Add category';
$this->params['breadcrumbs'][] = $this->title;
?>
<h3>Category form</h3>

<?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'cat_name')->textInput(['placeholder' => 'Category', 'style'=>'width:300px'])?>
   <?= $form->field($model, 'parent_id')->dropDownList($arr, ['style'=>'width:300px'])?>
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary'])?>
<?php ActiveForm::end() ?>

