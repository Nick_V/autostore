<?php
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use app\models\Product;

$this->title = $product;
$this->params['breadcrumbs'][] = $this->title;

echo GridView::widget([
            'dataProvider' => Product::Search($product),
]);

