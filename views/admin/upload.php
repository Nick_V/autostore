<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Upload';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if($result != "")
    {
        echo $result;
    } 
?>
<?php $form = ActiveForm::begin(['method' => 'post', 'options' => ['enctype'=>'multipart/form-data']]); ?>
    <input type="file" name="myfile"/>
    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
<?php $form = ActiveForm::end(); ?>
