<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Admin';
$this->params['breadcrumbs'][] = $this->title;

?>

<div >
  <h1><?= Html::encode($this->title) ?></h1>
  
  <ul class="list-group">
    <li class="list-group-item"><?= Html::a('Upload file', ['admin/upload']) ?></li>
    <li class="list-group-item"><?= Html::a('Update files', ['admin/getfiles']) ?></li>
    <li class="list-group-item"> <?= Html::a('Add new Category', ['admin/addcategory'], ['class' => 'btn btn-default']) ?></li>
  </ul>
</div>

