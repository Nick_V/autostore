<?php
use yii\helpers\Html;

$this->title = 'Update';
$this->params['breadcrumbs'][] = $this->title;
?>

<div>
  <h1><?= Html::encode($this->title) ?></h1>
  
  <ul class="list-group">
        <?php foreach ($files as $file): ?>
            <li class="list-group-item">
                <?php echo $file ?> 
                <?= Html::a('Update', ['admin/update', 'file' => $file], ['class' => 'btn btn-default']) ?>
            </li>
        <?php endforeach;?>
  </ul>
  <?= Html::a('Update all', ['admin/updateall'], ['class' => 'btn btn-default']) ?>
  
</div>


