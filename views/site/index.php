<?php
use app\models\Category;
use app\models\Product;
use app\repository\StoreService;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

$this->title = 'My Yii Application';

?>
<div class="site-index">

    <div class="jumbotron">
        <h1>AutoWebStore</h1>
    </div>

    <div class="body-content">
    <?php
    
        echo '<h3>'.'Categories'.'</h3>';
        StoreService::GetAllCategories();  
        
        echo '<h3>'.'Cars'.'</h3>';
        echo GridView::widget([
            'dataProvider' => Product::SearchAll(),
                
        ]);
    ?>
    </div>
</div>
