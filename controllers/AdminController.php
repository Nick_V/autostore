<?php
namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use SplFileInfo;
use app\models\Category;
use app\models\CategoryForm;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Product;
use app\repository\StoreService;
use yii\helpers\ArrayHelper;


class AdminController extends Controller
{
        
    public function actionIndex()
    {
        return $this->render('index');   
    }
    
    public function actionAddcategory()
    {
        $model = new CategoryForm;
        if($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $category = new Category();
            $category->parent_id = $model->parent_id;
            $category->cat_name = $model->cat_name;
            $category->save();
            
            return $this->redirect(['site/index']);
        }
        return $this->render('addcategory', ['model' => $model, 'arr' => StoreService::ArrCategories()]);
    }    
    
    public function actionGetproducts($product)
    {
        return $this->render('getproducts', ['product' => $product]);
    }
        
    public function actionUpload()
    {
        //StoreService::UploadFile();
        $myservice = new StoreService();
        $result = $myservice->UploadFile();
        return $this->render('upload',['result' => $result]);
    }
   
    public function actionGetfiles()
    {
        $myservice = new StoreService();
        $listfiles = $myservice->GetFiles();
        return $this->render('getfiles',['files' => $listfiles]);
    }
    
    public function actionUpdate($file)
    {
        StoreService::Update($file);
        return $this->redirect(['site/index']);
    }
    
    public function actionUpdateall()
    {
        StoreService::UpdateAll();
        return $this->redirect(['site/index']);
    }
    
}